package PT2018.Polynomial;

import java.util.ArrayList;
import java.util.List;

public class Model {
	public List <Monom> monoms = new ArrayList<Monom>();
	public List <Monom> monoms2 = new ArrayList<Monom>();
	public List <Monom> r = new ArrayList<Monom>();
	public List <Monom> q = new ArrayList<Monom>();
	
	public Model() {
		
	}
	public boolean isZero() {
		 for (Monom m : monoms2) if (m.getCoef() != 0) return false;
		 return true;
	}
	
	public boolean isOkay(String []s, int l1) {
		monoms.removeAll(monoms);
		for (int i = 0; i < l1; i++) {
			if (s[i].startsWith("-")) {
				for (char c : s[i].substring(1, s[i].length()).toCharArray()) if (!Character.isDigit(c)) return false;
				monoms.add(new Monom(Integer.parseInt(s[i]), l1 - i - 1));
			}
			else {
				for (char c : s[i].toCharArray()) if (!Character.isDigit(c)) return false;
				monoms.add(new Monom(Integer.parseInt(s[i]), l1 - i - 1));
			}
		}
		return true;
	}
	
	public List<Monom> multiply(List<Monom> m1, List<Monom> m2) {
		List<Monom> m = new ArrayList<Monom>();
		int h = m1.size() + m2.size() - 1;
		int ms1 = m1.size();
		int ms2 = m2.size();
		for (int i=0;i<h;i++) m.add(new Monom(0, h - i - 1));
		
		for(int i=0;i<ms1;i++) {
			for (int j=0;j<ms2;j++) {
				m.get(i+j).setCoef(m.get(i+j).getCoef() + m1.get(i).getCoef() * m2.get(j).getCoef());
			}
		}
		
		return m;
	}
	
	public List<Monom> multiply(Monom m1, List<Monom> m2) {
		List<Monom> m = new ArrayList<Monom>();
		int ms2 = m2.size();
		
		for (int i=0;i<ms2;i++) m.add(new Monom (0, ms2 - i - 1));
		
		for (int i=0;i<ms2;i++) {
			m.get(i).setCoef(m1.getCoef() * m2.get(i).getCoef());
			m.get(i).setGrade(m1.getGrade() + m2.get(i).getGrade());
		}
		
		return m;
	}
	
	public void divide() {
		r.removeAll(r);
		q.removeAll(q);
		int l1 = monoms.size(), l2 = monoms2.size(), rSize, hSize, s, grade;
		double coef;
		for (int i = 0; i < l1 ; i++) r.add(new Monom(monoms.get(i).getCoef(), monoms.get(i).getGrade())); //r<-n
		Monom t = new Monom();
		List <Monom> h = new ArrayList<Monom>();
		rSize = r.size();
		while (rSize != 0 && rSize >= l2) {
			s = 0;
			coef = r.get(0).getCoef() / monoms2.get(0).getCoef();
			grade = r.get(0).getGrade() - monoms2.get(0).getGrade();
			t.setCoef(coef);
			t.setGrade(grade);//t <- lead(r) / lead(d)
			q.add(new Monom(coef, grade)); //q<-q+t
			h.removeAll(h);
			h = multiply(t, monoms2);// t * d
			hSize = h.size();
			for (int i = 0; i < hSize; i++) {
				if (r.get(i - s).getCoef() == h.get(i).getCoef()) {
					r.remove(i - s);
					s++;
				}
				else r.get(i - s).setCoef(r.get(i - s).getCoef() - h.get(i).getCoef());
			}//r <- r - t * d, where t * d = h
			rSize = r.size();
		}
	}
	
	public void multiplyPolynomial(String criteria1, String criteria2) throws Exception {
		monoms2.removeAll(monoms2);
		String []str1 = criteria1.split(" "), str2 = criteria2.split(" ");
		int l1 = str1.length, l2 = str2.length;
		if (!isOkay(str1, l1)) throw new Exception("Wrong coef!");
		for (int i = 0; i < l1 ; i++) monoms2.add(new Monom(monoms.get(i).getCoef(), monoms.get(i).getGrade()));
		if (!isOkay(str2, l2)) throw new Exception("Wrong coef!");
		monoms = multiply(monoms, monoms2);
	}
	
	public void dividePolynomial(String criteria1, String criteria2) throws Exception {
		monoms2.removeAll(monoms2);
		String []str1 = criteria1.split(" "), str2 = criteria2.split(" ");
		int l1 = str1.length, l2 = str2.length;
		if (!isOkay(str2, l2)) throw new Exception("Wrong coef!");
		for (int i = 0; i < l2 ; i++) monoms2.add(new Monom(monoms.get(i).getCoef(), monoms.get(i).getGrade()));
		if (!isOkay(str1, l1)) throw new Exception("Wrong coef!");
		if (isZero()) throw new Exception("Divide by 0!");
		divide();
	}
	
	public void DerivatePolynomial(String criteria) throws Exception {
		monoms.removeAll(monoms);
		String []str = criteria.split(" ");
		int l = str.length;
		if (!isOkay(str, l)) throw new Exception("Wrong coef!");
		for (Monom m : monoms) {
			m.setCoef(m.getCoef() * m.getGrade());
			m.setGrade(m.getGrade() - 1);
		}
	}
	
	public void IntegratePolynomial(String criteria) throws Exception {
		monoms.removeAll(monoms);
		String []str = criteria.split(" ");
		int l = str.length;
		if (!isOkay(str, l)) throw new Exception("Wrong coef!");
		for (Monom m : monoms) {
			m.setGrade(m.getGrade() + 1);
			m.setCoef(m.getCoef() / m.getGrade());
		}
	}
	
	public void addPolynomial(String criteria1, String criteria2) throws Exception {
		monoms.removeAll(monoms);
		String []str1 = criteria1.split(" "), str2 = criteria2.split(" ");
		int l1 = str1.length, l2 = str2.length;
		if (l1 > l2) {
			if (!isOkay(str1, l1)) throw new Exception("Wrong coef!");
			for (int i = 0; i < l2; i++) {
				if (str2[i].startsWith("-")) {
					for (char c : str2[i].substring(1, str2[i].length()).toCharArray()) if (!Character.isDigit(c)) throw new Exception("Wrong coef!");
					monoms.get(l1 - l2 + i).setCoef(monoms.get(l1 - l2 + i).getCoef() + Integer.parseInt(str2[i]));
				}
				else {
					for (char c : str2[i].toCharArray()) if (!Character.isDigit(c)) throw new Exception("Wrong coef!");
					monoms.get(l1 - l2 + i).setCoef(monoms.get(l1 - l2 + i).getCoef() + Integer.parseInt(str2[i]));
				}
			}
		}
		else {
			if (!isOkay(str2, l2)) throw new Exception("Wrong coef!");
			for (int i = 0; i < l1; i++) {
				if (str1[i].startsWith("-")) {
					for (char c : str1[i].substring(1, str1[i].length()).toCharArray()) if (!Character.isDigit(c)) throw new Exception("Wrong coef!");
					monoms.get(l2 - l1 + i).setCoef(monoms.get(l2 - l1 + i).getCoef() + Integer.parseInt(str1[i]));
				}
				else {
					for (char c : str1[i].toCharArray()) if (!Character.isDigit(c)) throw new Exception("Wrong coef!");
					monoms.get(l2 - l1 + i).setCoef(monoms.get(l2 - l1 + i).getCoef() + Integer.parseInt(str1[i]));
				}
			}
		}
	}
	
	public void subtractPolynomial(String criteria1, String criteria2) throws Exception {
		monoms.removeAll(monoms);
		String []str1 = criteria1.split(" "), str2 = criteria2.split(" ");
		int l1 = str1.length, l2 = str2.length;
		if (!isOkay(str1, l1)) throw new Exception("Wrong coef!");
		for (int i = 0; i < l2; i++) {
			if (str2[i].startsWith("-")) {
				for (char c : str2[i].substring(1, str2[i].length()).toCharArray()) if (!Character.isDigit(c)) throw new Exception("Wrong coef!");
				monoms.get(l1 - l2 + i).setCoef(monoms.get(l1 - l2 + i).getCoef() - Integer.parseInt(str2[i]));
			}
			else {
				for (char c : str2[i].toCharArray()) if (!Character.isDigit(c)) throw new Exception("Wrong coef!");
				monoms.get(l1 - l2 + i).setCoef(monoms.get(l1 - l2 + i).getCoef() - Integer.parseInt(str2[i]));
			}
		}
	}
}

