package PT2018.Polynomial;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Framing{
	private JFrame mainFrame;
	private JTextField polynomialInputFirst;
	private JTextField polynomialInputSecond;
	private JTextField polynomialInput;
	private JTextArea resultOutput;
	private JButton addButton = new JButton("Add");
	private	JButton subtractButton = new JButton("Subtract");
	private JButton multiplyButton = new JButton("Multiply");
	private JButton divideButton = new JButton("Divide");
	private JButton clearButton = new JButton("Clear");
	private JButton integrateButton = new JButton("Integrate");
	private JButton derivateButton = new JButton("Derivate");
	
	public Framing() {
		prepareGUI();
	}
	
	private void prepareGUI() {
		JLabel polynomialFirst;
		JLabel polynomialSecond;
		JLabel polynomial;
		JLabel result;
		JPanel panel1;
		JPanel panel2;
		JPanel panel3;
		
		mainFrame = new JFrame("Polynomial calculator");
		mainFrame.setSize(500,  500);
		mainFrame.setLayout(new GridLayout(3, 1));
		
		polynomialFirst = new JLabel("Coefficients of the first polynomial: ", JLabel.CENTER);
		polynomialSecond = new JLabel("Coefficients of the second polynomial: ", JLabel.CENTER);
		polynomial = new JLabel("Coefficients of the polynomial to derivate/integrate: ", JLabel.CENTER);
		
		result = new JLabel("Result:", JLabel.CENTER);
		
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		
		polynomialInputFirst = new JTextField(40);
		polynomialInputSecond = new JTextField(40);
		polynomialInput = new JTextField(40);
		
		panel1 = new JPanel();
		//panel1.setLayout(new GridLayout(2, 1));
		panel1.add(polynomialFirst);
		panel1.add(polynomialInputFirst);
		panel1.add(polynomialSecond);
		panel1.add(polynomialInputSecond);
		panel1.add(addButton);
		panel1.add(subtractButton);
		panel1.add(multiplyButton);
		panel1.add(divideButton);
		panel1.add(clearButton);
		
		panel2 = new JPanel();
		panel2.add(polynomial);
		panel2.add(polynomialInput);
		panel2.add(derivateButton);
		panel2.add(integrateButton);
		

		resultOutput = new JTextArea();
		resultOutput.setRows(2);
		resultOutput.setColumns(40);
		resultOutput.setEditable(false);
		
		panel3 = new JPanel();
		panel3.add(result);
		panel3.add(resultOutput);
		
		mainFrame.add(panel1);
		mainFrame.add(panel2);
		mainFrame.add(panel3);
		
		mainFrame.setVisible(true);
	}

	void addListener(ActionListener evt) {
		addButton.addActionListener(evt);
		subtractButton.addActionListener(evt);
		multiplyButton.addActionListener(evt);
		divideButton.addActionListener(evt);
		integrateButton.addActionListener(evt);
		derivateButton.addActionListener(evt);
		clearButton.addActionListener(evt);
	}

	public JTextField getPolynomialInputFirst() {
		return polynomialInputFirst;
	}

	public JTextField getPolynomialInputSecond() {
		return polynomialInputSecond;
	}

	public JTextField getPolynomialInput() {
		return polynomialInput;
	}

	public JTextArea getResultOutput() {
		return resultOutput;
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

}

