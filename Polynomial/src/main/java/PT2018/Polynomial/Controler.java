package PT2018.Polynomial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.List;

public class Controler {
	private Framing view;
	private Model model;
	Controler(Framing view, Model model) {
		this.model = model;
		this.view = view;
		
		view.addListener(new ButtonClickListener());
	}
	
	private class ButtonClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			
			if (command.equals("Clear")) {
				clearInput();
			}
			else if(command.equals("Integrate")) {
				integrate();
			}
			else if(command.equals("Derivate")){
				derivate();
			}
			else if(command.equals("Add")){
				justAdd();
			}
			else if(command.equals("Subtract")){
				subtract();
			}
			else if(command.equals("Multiply")){
				multiply();
			}
			else if(command.equals("Divide")){
				divide();
			}
			else {
				System.out.println("Something is wrong!");
			}
		}

		public void display(List <Monom> helpin) {
			double coef;
			int grade;
			int l = helpin.size();
			for (int i = 0; i < l; i++) {
				grade = helpin.get(i).getGrade();
				coef = helpin.get(i).getCoef();
				if (coef != 0) {
					if (coef > 0 && i!=0) view.getResultOutput().append("+");
					if (grade == 0) view.getResultOutput().append(coef + "");
					else if (grade == 1) view.getResultOutput().append(coef + "x");
					else view.getResultOutput().append(coef + "x^" + grade);
				}
			}
		}

		private void divide() {
			String p1 = view.getPolynomialInputFirst().getText();
			String p2 = view.getPolynomialInputSecond().getText();
			try {
				model.dividePolynomial(p1, p2);
			} catch(Exception e1) {
				JOptionPane.showMessageDialog(view.getMainFrame(), "Something is wrong with the division!");
			}
			view.getResultOutput().setText("");
			display(model.monoms);
			view.getResultOutput().append(" = (");
			display(model.monoms2);
			view.getResultOutput().append(") * (");
			display(model.q);
			view.getResultOutput().append(") + ");
			display(model.r);
		}

		private void multiply() {
			String p1 = view.getPolynomialInputFirst().getText();
			String p2 = view.getPolynomialInputSecond().getText();
			try {
				model.multiplyPolynomial(p1, p2);
			} catch(Exception e1) {
				JOptionPane.showMessageDialog(view.getMainFrame(), "Something is wrong with the multiplication!");
			}
			view.getResultOutput().setText("");
			display(model.monoms);
		}

		private void subtract() {
			String p1 = view.getPolynomialInputFirst().getText();
			String p2 = view.getPolynomialInputSecond().getText();
			try {
				model.subtractPolynomial(p1, p2);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(view.getMainFrame(), "Something is wrong with the subtraction!");
			}
			view.getResultOutput().setText("");
			display(model.monoms);
		}

		private void justAdd() {
			String p1 = view.getPolynomialInputFirst().getText();
			String p2 = view.getPolynomialInputSecond().getText();
			try {
				model.addPolynomial(p1, p2);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(view.getMainFrame(), "Something is wrong with the addition!");
			}
			view.getResultOutput().setText("");
			display(model.monoms);
		}

		private void derivate() {
			String p = view.getPolynomialInput().getText();
			try {
				model.DerivatePolynomial(p);
			} catch(Exception e1) {
				JOptionPane.showMessageDialog(view.getMainFrame(), "Something is wrong with the derivation!");
			}
			view.getResultOutput().setText("");
			display(model.monoms);
		}

		private void integrate() {
			String p = view.getPolynomialInput().getText();
			try {
				model.IntegratePolynomial(p);
			} catch(Exception e1) {
				JOptionPane.showMessageDialog(view.getMainFrame(), "Something is wrong with the integration!");
			}
			view.getResultOutput().setText("");
			display(model.monoms);
		}

		private void clearInput() {
			view.getPolynomialInput().setText("");
			view.getPolynomialInputFirst().setText("");
			view.getPolynomialInputSecond().setText("");
			view.getResultOutput().setText("");
		}
	}

}

