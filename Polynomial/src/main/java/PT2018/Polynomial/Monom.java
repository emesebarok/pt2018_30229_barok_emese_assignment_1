package PT2018.Polynomial;

public class Monom {
	private double coef;
	private int grade;
	
	public Monom() {
		this.coef = 0;
		this.grade = 0;
	}
	
	public Monom(double coef, int grade) {
		this.coef = coef;
		this.grade = grade;
	}
	
	public Monom(Monom m) {
		this.coef = m.getCoef();
		this.grade = m.getGrade();
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}
}
